import Vue from "vue";
import Vuex from "vuex";


Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    carrinho: [],
    login: false,
    conta: {}
  },
  mutations: {
    carrinho(state, carrinho) {
      state.carrinho = carrinho;
      window.sessionStorage.setItem("carrinho", JSON.stringify(carrinho));
    },
    saldo(state, saldo) {
      state.conta.saldo = saldo;
      window.sessionStorage.setItem("conta", JSON.stringify(state.conta));
    },
    login(state, conta) {
      state.login = true;
      state.conta = conta;
      window.sessionStorage.setItem("login", true);
      window.sessionStorage.setItem("conta", JSON.stringify(conta));
    },
    logOut(state) {
      state.login = false;
      state.conta = {};
      window.sessionStorage.setItem("login", false);
      window.sessionStorage.removeItem("conta");
      window.sessionStorage.removeItem("carrinho");
    }
  },
  getters: {
    carrinho: state =>
      state.carrinho || JSON.parse(window.sessionStorage.getItem("carrinho")),
    login: state =>
      state.login || JSON.parse(window.sessionStorage.getItem("login")),
    conta: state =>
      state.conta || JSON.parse(window.sessionStorage.getItem("conta"))
  }
});
