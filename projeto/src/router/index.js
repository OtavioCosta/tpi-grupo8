import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import Home from "../views/Home.vue";
import CadastroLojas from "../views/CadastroLojas.vue";
import CadastroClientes from "../views/CadastroClientes.vue";
import CadastroProdutos from "../views/CadastroProdutos.vue";
import CadastroCartao from "../views/CadastroCartao.vue";
import AdicionarSaldo from "../views/AdicionarSaldo.vue";
import Carrinho from "../views/Carrinho.vue";
import NotFound from "../views/NotFound.vue";


Vue.use(VueRouter);

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login
  },
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/cadastrolojas",
    name: "CadastroLojas",
    component: CadastroLojas
  },
  {
    path: "/cadastroclientes",
    name: "CadastroClientes",
    component: CadastroClientes
  },
  {
    path: "/cadastroprodutos",
    name: "CadastroProdutos",
    component: CadastroProdutos,
    beforeEnter(to, from, next) {
      const conta = JSON.parse(sessionStorage.getItem("conta"));
      if (conta.tipoConta === "cliente") next({ name: "Home" });
      else next();
    }
  },
  {
    path: "/cadastrocartao",
    name: "CadastroCartao",
    component: CadastroCartao,
    beforeEnter(to, from, next) {
      const conta = JSON.parse(sessionStorage.getItem("conta"));
      if (conta.tipoConta !== "cliente") next({ name: "CadastroProdutos" });
      else next();
    }
  },
  {
    path: "/adicionarsaldo",
    name: "AdicionarSaldo",
    component: AdicionarSaldo,
    beforeEnter(to, from, next) {
      const conta = JSON.parse(sessionStorage.getItem("conta"));
      if (conta.tipoConta !== "cliente") next({ name: "CadastroProdutos" });
      else next();
    }
  },
  {
    path: "/carrinho",
    name: "carrinho",
    component: Carrinho,
    beforeEnter(to, from, next) {
      const conta = JSON.parse(sessionStorage.getItem("conta"));
      if (conta.tipoConta !== "cliente") next({ name: "CadastroProdutos" });
      else next();
    }
  },
  {
    path: "*",
    name: "notFound",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  const login = JSON.parse(sessionStorage.getItem("login"));
  if (
    to.name !== "CadastroClientes" &&
    to.name !== "CadastroLojas" &&
    to.name !== "Login" &&
    !login
  )
    next({ name: "Login" });
  else next();
});

export default router;
